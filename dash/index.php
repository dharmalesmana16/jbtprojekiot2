
<?php
session_start();
if (empty($_SESSION['username'])) {
    echo "<script>alert('Maaf, untuk mengakses halaman ini, anda harus login terlebih dahulu, terima kasih');document.location='../index.php'</script>";
}
if (isset($_SESSION['username'])){
$_SESSION['login_user_time']=time();
}
// i
?>
<?php
include '../koneksi.php';
$sql = mysqli_query($koneksi, "SELECT * FROM setting ");
$dapat= mysqli_fetch_array($sql);
$max1=$dapat['kec1'];
$max2=$dapat['kec2'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Monitoring Angin</title>
  <link rel="shortcut icon" href="Jasamarga_Bali.ico" type="image/x-icon">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../dist/css/nav.css">
  <script src="https://www.cssscript.com/demo/canvas-based-html5-gauge-library-gauge-js/gauge.min.js"></script>
</head>
<body class="hold-transition sidebar-collapse layout-fixed layout-navbar-fixed layout-footer-fixed ">
    
<div class="wrapper">
  <!-- Navbar -->

  <nav class="main-header navbar navbar-expand-sm justify-content-center text-light navbar-dark">
      <div class="row">
          &ensp;<h4 class="font-weight-normal" style="font-family:'Orbitron'">MONITORING KECEPATAN ANGIN DAN CUACA TOL BALI MANDARA</h4>
      </div>

    <!-- Left navbar links -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ">
      <li class="nav-item">
        <a class="nav-link" id="full" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid ">
        <!-- Info laporan -->
        <div class="row">
          
            <section class="col-lg-6">
             
                <?php
                function ping($host){
                  $str = exec("ping -n 1 -w 1 $host", $input, $result);
                  if ($result == 0){
                      return '<span class="font-weight-normal text-green" style="font-size:16px;"> On </span><br>';
                  }else{
                      return '<span class="font-weight-normal text-danger"> Off </span><br>';
                  }
                  
                }
                ?>
                
              <div class="card shadow-lg  " style="background-color:#1e272e">
                <div class="row">
                  <div class="col-lg-2 text-success" >
                    <h5 style="font-size:14px">&ensp;Location : Ngurah Rai</h5>
                    <h5 style="font-size:14px">&ensp;Network Status : <?php echo ping('192.168.1.1')?></h5>
                    <h5 style="font-size:14px">&ensp;Coordinate :</h5>
                    
                  </div>
                  <div class="col-lg-7">
                  </div>
                  <div class="col-lg-3 text-light">
                    <h5 id ="kecepatanangka" class="display-1" style="font-family:'Orbitron';"></h5>
                    <p style="font-size:40px">KM/JAM</p>
                  </div>
                </div>
                <center>
                
                <canvas id="kecepatan"> </canvas>
                <canvas id="kompas"> </canvas>
                <br>
                <canvas id="suhu"></canvas>
                <canvas id="kelembaban"></canvas>
                <center>
                <h5 class="text-right text-success" style="font-size:14px;">Server Status : <?php echo ping('192.168.1.40')?></h5>

              </div>
            </section>

            <section class="col-lg-6">
              <div class="card card-warning rounded" style="background-color:#1e272e">
              <div class="row">
                  <div class="col-lg-2 text-success" >
                    <h5 style="font-size:14px">&ensp;Location : Nusa Dua</h5>
                    <h5 style="font-size:14px">&ensp;Network Status : <?php echo ping('192.168.1.1')?></h5>
                    <h5 style="font-size:14px">&ensp;Coordinate :</h5>
                  </div>
                  <div class="col-lg-7">
                  </div>
                  <div class="col-lg-3 text-light">
                    <h5 id ="kecepatanangka2" class="display-1" style="font-family:'Orbitron';"></h5>
                    <p style="font-size:40px">KM/JAM</p>
                  </div>
                </div>
              <center>
                  
                  <canvas id="kecepatan2" data-font-value="Orbitron"
        data-font-numbers="Orbitron"
        data-font-title="Orbitron"
        data-font-units="Orbitron"></canvas>
                  <canvas id="kompas2"> </canvas>
                  <br>
                  <canvas id="suhu2"></canvas>
                  <canvas id="kelembaban2"></canvas>
                </center>
                <h5 class="text-right text-success" style="font-size:14px;">Server Status : <?php echo ping('192.168.1.40')?></h5>
              </div>
              
            </section>
        </div>
        <!-- graf konten -->
  <!-- /.control-sidebar -->
  <?php include '../footer.php'?>
  <!-- Main Footer -->

</div>    
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>
<script src="../plugins/sweetalert2/sweetalert2.all.min.js"></script>
<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="../plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../plugins/raphael/raphael.min.js"></script>
<!-- ChartJS -->
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
<script>
var gaugekompas1 = new RadialGauge({
    renderTo: 'kompas',
    width: 250,
    height:250,
    minValue: 0,
    maxValue: 360,
    majorTicks: [
        "Utara",
        "Timur Laut",
        "Barat",
        "Tenggara",
        "Selatan",
        "Barat Daya",
        "Timur",
        "Barat Laut",
        "Utara"
    ],
    minorTicks: 22,
    ticksAngle: 360,
    startAngle: 180,
    strokeTicks: false,
    highlights: false,
    colorPlate: "#222",
    colorMajorTicks: "red",
    colorMinorTicks: "white",
    colorNumbers: "green",
    colorNeedle: "rgba(240, 128, 128, 1)",
    colorNeedleEnd: "rgba(255, 160, 122, .9)",
    valueBox: false,
    valueTextShadow: false,
    colorCircleInner: "#fff",
    colorNeedleCircleOuter: "#ccc",
    needleCircleSize: 15,
    needleCircleOuter: false,
    animationRule: "linear",
    needleType: "arrow",
    needleStart: 75,
    needleEnd: 99,
    needleWidth: 3,
    borders: true,
    borderInnerWidth: 0,
    borderMiddleWidth: 0,
    borderOuterWidth: 10,
    colorBorderOuter: "yellow",
    colorBorderOuterEnd: "#ccc",
    colorNeedleShadowDown: "#222",
    borderShadowWidth: 0,
    animationDuration: 1500
}).draw();
//pemisah
  var gaugesuhu1 = new RadialGauge({
    renderTo: 'suhu',
    width: 200,
    height: 200,
    units: "°C",
    title: "Temperature",
    minValue: -50,
    maxValue: 60,
    majorTicks: [
        -50,
        -40,
        -30,
        -20,
        -10,
        0,
        10,
        20,
        30,
        40,
        50,
        60
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": -50,
            "to": 0,
            "color": "rgba(0,0, 255, .3)"
        },
        {
            "from": 0 ,
            "to":30 ,
            "color": "rgba(0, 255, 0, .3)"
        },
        {
            "from": 30,
            "to":60 ,
            "color": "rgba(255, 0, 0, .3)"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#222",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#333",
    colorBorderOuterEnd: "#111",
    colorBorderMiddle: "#222",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#111",
    colorBorderInnerEnd: "#333",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "#333",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 0,
    colorValueBoxRect: "#222",
    colorValueBoxRectEnd: "#333"
}).draw();
//pemisah
  var gaugekecepatan1 = new RadialGauge({
    renderTo: 'kecepatan',
    width: 450,
    height: 450,
    units: "Km/h",
    title: "Wind Speed",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
      0,
      10,
      20,
      30,
      40,
      50,
      60,
      70,
      80,
      90,
      100
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 20,
            "color": "#1dd1a1"
        },
        {
            "from": 20 ,
            "to":30 ,
            "color": "#fbc531"
        },
        {
            "from": 30,
            "to":60 ,
            "color": "#EE5A24"
        },
        {
            "from": 60,
            "to":100 ,
            "color": "#EA2027"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#1e272e",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#ffa801 ",
    colorBorderOuterEnd: "#0652DD ",
    colorBorderMiddle: "#ffa801",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#0652DD ",
    colorBorderInnerEnd: "#222",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "white",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 10,
    colorValueBoxRect: "#ffa801",
    colorValueBoxRectEnd: "#ffa801",
    
}).draw();
// pemisah
  var gaugekelembaban1 = new RadialGauge({
    renderTo: 'kelembaban',
    width: 200,
    height: 200,
    units: "%",
    title: "Kelembaban",
    minValue: 0,
    maxValue: 100,
    
    majorTicks: [0,10,20,30,40,50,60,70,80,90,100],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 100,
            "color": "rgba(255, 0, 0, .3)"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#222",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#333",
    colorBorderOuterEnd: "#111",
    colorBorderMiddle: "#222",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#111",
    colorBorderInnerEnd: "#333",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "#333",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 0,
    colorValueBoxRect: "#222",
    colorValueBoxRectEnd: "#333"
}).draw();
//pemisah
var gaugekompas2 = new RadialGauge({
    renderTo: 'kompas2',
    width: 250,
    height:250,
    minValue: 0,
    maxValue: 360,
    majorTicks: [
        "Utara",
        "Timur Laut",
        "Barat",
        "Tenggara",
        "Selatan",
        "Barat Daya",
        "Timur",
        "Barat Laut",
        "Utara"
    ],
    minorTicks: 22,
    ticksAngle: 360,
    startAngle: 180,
    strokeTicks: false,
    highlights: false,
    colorPlate: "#222",
    colorMajorTicks: "red",
    colorMinorTicks: "white",
    colorNumbers: "green",
    colorNeedle: "rgba(240, 128, 128, 1)",
    colorNeedleEnd: "rgba(255, 160, 122, .9)",
    valueBox: false,
    valueTextShadow: false,
    colorCircleInner: "#fff",
    colorNeedleCircleOuter: "#ccc",
    needleCircleSize: 15,
    needleCircleOuter: false,
    animationRule: "linear",
    needleType: "arrow",
    needleStart: 75,
    needleEnd: 99,
    needleWidth: 3,
    borders: true,
    borderInnerWidth: 0,
    borderMiddleWidth: 0,
    borderOuterWidth: 10,
    colorBorderOuter: "yellow",
    colorBorderOuterEnd: "#ccc",
    colorNeedleShadowDown: "#222",
    borderShadowWidth: 0,
    animationDuration: 1500
}).draw();
//pemisah
  var gaugesuhu2 = new RadialGauge({
    renderTo: 'suhu2',
    width: 200,
    height: 200,
    units: "°C",
    title: "Temp",
    minValue: -50,
    maxValue: 60,
    majorTicks: [-50,-40,-30,-20,-10,0,10,20,30,40,50,60],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
      {
            "from": -50,
            "to": 0,
            "color": "rgba(0,0, 255, .3)"
        },
        {
            "from": 0 ,
            "to":30 ,
            "color": "rgba(0, 255, 0, .3)"
        },
        {
            "from": 30,
            "to":60 ,
            "color": "rgba(255, 0, 0, .3)"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#222",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#333",
    colorBorderOuterEnd: "#111",
    colorBorderMiddle: "#222",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#111",
    colorBorderInnerEnd: "#333",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "#333",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 0,
    colorValueBoxRect: "#222",
    colorValueBoxRectEnd: "#333"
}).draw();
//pemisah
  var gaugekecepatan2 = new RadialGauge({
    renderTo: 'kecepatan2',
    width: 450,
    height: 450,
    units: "Km/h",
    title: "Wind Speed",
    minValue: 0,
    maxValue: 100,
    majorTicks: [
      0,
      10,
      20,
      30,
      40,
      50,
      60,
      70,
      80,
      90,
      100
    ],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 20,
            "color": "#1dd1a1"
        },
        {
            "from": 20 ,
            "to":30 ,
            "color": "#fbc531"
        },
        {
            "from": 30,
            "to":60 ,
            "color": "#EE5A24"
        },
        {
            "from": 60,
            "to":100 ,
            "color": "#EA2027"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#1e272e",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#ffa801 ",
    colorBorderOuterEnd: "#0652DD ",
    colorBorderMiddle: "#ffa801",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#0652DD ",
    colorBorderInnerEnd: "#222",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "white",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 10,
    colorValueBoxRect: "#ffa801",
    colorValueBoxRectEnd: "#ffa801",
    
}).draw();
//pemisah
  var gaugekelembaban2 = new RadialGauge({
    renderTo: 'kelembaban2',
    width: 200,
    height: 200,
    units: "%",
    title: "Kelembaban",
    minValue: 0,
    maxValue: 100,
    majorTicks: [0,10,20,30,40,50,60,70,80,90,100],
    minorTicks: 2,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 100,
            "color": "rgba(255, 0, 0, .3)"
        }
    ],
    ticksAngle: 225,
    startAngle: 67.5,
    colorMajorTicks: "#ddd",
    colorMinorTicks: "#ddd",
    colorTitle: "#eee",
    colorUnits: "#ccc",
    colorNumbers: "#eee",
    colorPlate: "#222",
    borderShadowWidth: 0,
    borders: true,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    colorBorderOuter: "#333",
    colorBorderOuterEnd: "#111",
    colorBorderMiddle: "#222",
    colorBorderMiddleEnd: "#111",
    colorBorderInner: "#111",
    colorBorderInnerEnd: "#333",
    colorNeedleShadowDown: "#333",
    colorNeedleCircleOuter: "#333",
    colorNeedleCircleOuterEnd: "#111",
    colorNeedleCircleInner: "#111",
    colorNeedleCircleInnerEnd: "#222",
    valueBoxBorderRadius: 0,
    colorValueBoxRect: "#222",
    colorValueBoxRectEnd: "#333"
}).draw();
var kecepatanangka = document.getElementById("kecepatanangka");
var kecepatanangka2 = document.getElementById("kecepatanangka2");
const data= 'fun.php';
$(document).ready(function() {
    selesai();
});
 
function selesai() {
	setTimeout(function() {
		gauge();
		selesai();
	}, 2000);
}
async function gauge(){
    const datbar = await fetch(data);
    const jadi = await datbar.json();
    gaugekompas1.value=jadi[0]['arah'];
    gaugekompas2.value=jadi[0]['arah2'];
    gaugesuhu1.value=jadi[0]['suhu'];
    gaugesuhu2.value=jadi[0]['suhu2'];
    gaugekecepatan1.value=jadi[0]['kecepatan'];
    gaugekecepatan2.value=jadi[0]['kecepatan2'];
    gaugekelembaban1.value=jadi[0]['kelembaban'];
    gaugekelembaban2.value=jadi[0]['kelembaban2'];
    kecepatanangka.innerHTML=jadi[0]['kecepatan'] ;
    kecepatanangka2.innerHTML=jadi[0]['kecepatan2'];
    var dat1=<?= (int)$max1 ?> ;
    var dat2 =<?= (int)$max2 ?>;
    if (jadi[0]['kecepatan']>= dat1 || jadi[0]['kecepatan2']>=dat2) {
      Swal.fire({
        title: 'Warning !!!',
        text: 'Bahaya Kecepatan Angin',
        icon: 'error',
        confirmButtonText: 'STOP'
      });
      var audio = new Audio('alarm.mp3');
      audio.play();
    }
}
</script>
</html>