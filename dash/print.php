<?php
session_start();
if (empty($_SESSION['role'])) {
    echo "<script>alert('Maaf, untuk mengakses halaman ini, anda harus login terlebih dahulu, terima kasih');document.location='../index.php'</script>";
}
if ($_SESSION['role']=="admin"){
$_SESSION['login_user_time']=time();
}
else {
  echo "<script>alert('Maaf, anda tidak dapat mengakses halaman ini sebagai user, terima kasih');document.location='../index.php'</script>";
}
// i
?>
<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
include "../koneksi.php";
$tgl1 = @$_POST['tanggal'];
$a=@$_POST['tanggal2'];
$b=explode("-",$a);
// $b[2]+=1;
$tgl2="$b[0]-$b[1]-$b[2]";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laporan Monitoring</title>
  <link rel="shortcut icon" href="Jasamarga_Bali.ico" type="image/x-icon">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
</head>
<script type="text/javascript">
$(document).ready(function() {
    $('#waktu1').timepicker({
        showPeriodLabels: false
    });
    $('#waktu2').timepicker({
        showPeriodLabels: false
    });
  });
</script>
<body class="hold-transition sidebar-collapse">
<div class="wrapper">
  <!-- Navbar -->
  <?php include '../header.php' ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Monitoring Lokasi Ngurah Rai</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                    <form action="print.php" method="post">
                      <p>
                      <input type="date" name="tanggal" required/>
                          to
                      <input type="date" name="tanggal2" required/>
                          <button id=tom1 weight=100 height=100 type="submit" value="kirim" class="btn btn-success btn-sm">Cetak Data</button>
	                   </p>
                     
                    </form>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th><center>Tanggal</center></th>
                    <th><center>Arah Mata Angin</center></th>
                    <th><center>Kecepatan Angin</center></th>
                    <th><center>Suhu Udara</center></th>
                    <th><center>Kelembabab Udara</center></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
            error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
            try {
                $cek_data = mysqli_query($koneksi, "SELECT count(tanggal) FROM data WHERE tanggal between '$tgl1' and '$tgl2' ORDER BY tanggal");
                $dapat=mysqli_fetch_array($cek_data);
                $dt=$dapat['count(tanggal)'];
            if ($dt==0) {
                $eto1=0;
                echo "<tr>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    </tr>";
            }
            else{
                $sql =mysqli_query($koneksi, "SELECT * FROM data WHERE tanggal between '$tgl1' and '$tgl2' ORDER BY tanggal ");
                foreach ($sql as $key => $row) {
                  $arah=$row['arah'];
                $kecepatan=$row['kecepatan'];
                $suhu=$row['suhu'];
                $kelembaban=$row['kelembaban'];
                $tanggal=$row['tanggal'];
                echo "<tr>
                        <td><center>".$tanggal."</center></td>
                        <td><center>".$arah."</center></td>
                        <td><center>".$kecepatan."</center></td>
                        <td><center>".$suhu."</center></td>
                        <td><center>".$kelembaban."</center></td>
                        </center>
                        </tr>";
                    }
                }
                
                  } catch (Exception $e) {
                    //throw $th;
                }
                ?>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Monitoring Lokasi Nusa Dua</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                    <!-- <form action="print.php" method="post">
                      <p>
                      <input type="date" name="tanggal" required/>
                          to
                      <input type="date" name="tanggal2" required/>
                          <button id=tom2 weight=100 height=100 type="submit" value="kirim" class="btn btn-success btn-sm">Cetak Data</button>
	                   </p>
                     
                    </form> -->
                <table  class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th><center>Tanggal</center></th>
                    <th><center>Arah Mata Angin</center></th>
                    <th><center>Kecepatan Angin</center></th>
                    <th><center>Suhu Udara</center></th>
                    <th><center>Kelembabab Udara</center></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
            error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
            try {
                $cek_data = mysqli_query($koneksi, "SELECT count(tanggal) FROM data WHERE tanggal between '$tgl1' and '$tgl2' ORDER BY tanggal");
                $dapat=mysqli_fetch_array($cek_data);
                $dt=$dapat['count(tanggal)'];
            if ($dt==0) {
                $eto1=0;
                echo "<tr>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    <td><center>"."0"."</center></td>
                    </tr>";
            }
            else{
                $sql =mysqli_query($koneksi, "SELECT * FROM data WHERE tanggal between '$tgl1' and '$tgl2' ORDER BY tanggal ");
                foreach ($sql as $key => $row) {
                  $arah=$row['arah2'];
                $kecepatan=$row['kecepatan2'];
                $suhu=$row['suhu2'];
                $kelembaban=$row['kelembaban2'];
                $tanggal=$row['tanggal'];
                echo "<tr>
                        <td><center>".$tanggal."</center></td>
                        <td><center>".$arah."</center></td>
                        <td><center>".$kecepatan."</center></td>
                        <td><center>".$suhu."</center></td>
                        <td><center>".$kelembaban."</center></td>
                        </center>
                        </tr>";
                    }
                }
                
                  } catch (Exception $e) {
                    //throw $th;
                }
                ?>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
      
    </section>
   
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <?php include '../footer.php'?>
</div>

<!-- ./wrapper -->


<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../plugins/jszip/jszip.min.js"></script>
<script src="../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(document).Toasts('create', {
  class: 'bg-warning',
  title: 'Cara penggunaan',
  body: 'Masukan Range tanggal terlebih dahulu, kemudian klik cetak data, selanjutnya anda bisa memfilter data dengan memasukan keyword data yang anda cari pada search box'
});
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
  // $(function () {
  //   $("#example2").DataTable({
  //     "responsive": true, "lengthChange": false, "autoWidth": false,
  //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
  //   }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
  // });
</script>
</body>
</html>
