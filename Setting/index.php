<?php 
session_start();
if (empty($_SESSION['username'])) {
    echo "<script>alert('Maaf, untuk mengakses halaman ini, anda harus login terlebih dahulu, terima kasih');document.location='../index.php'</script>";
}
if ($_SESSION['role']=="admin"){
$_SESSION['login_user_time']=time();
}
else {
  echo "<script>alert('Maaf, untuk mengakses halaman ini, anda harus login sebagai admin  terlebih dahulu, terima kasih');document.location='../index.php'</script>";

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Monitoring Angin</title>
  <link rel="shortcut icon" href="Jasamarga_Bali.ico" type="image/x-icon">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../dist/css/nav.css">
  <script src="https://www.cssscript.com/demo/canvas-based-html5-gauge-library-gauge-js/gauge.min.js"></script>
</head>
<body class="hold-transition  sidebar-collapse layout-fixed layout-navbar-fixed layout-footer-fixed">
    
<div class="wrapper">
  <!-- Navbar -->
  <?php include '../header.php'?>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
   <div class="row">
    <div class="col-lg-2">

    </div>
      <section class='col-lg-4'>
              <div class='card card-danger'>
                <div  class='card-header'>
                  <h3 id=laporan class='card-title'>Konfigurasi Lokasi Ngurah Rai</h3>
                  <div class='card-tools'>
                </div>
                </div>
                <div class='card-body '>
                  <form action='../ng.php' method='post'>
                  <label class="text-left">Masukkan batas kecepatan angin</label>
                  <div class="input-group ">
                   <input type="number" class="form-control" name="kec" placeholder="Masukkan Batas Kecepatan Angin Dalam Angka" required>
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-wind"></span>
                      </div>
                      <br>
                    </div>
                   </div>
                  <div>
                   <br>
                        <button type='submit' class='btn btn-primary float-right'>Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </section>

            <section class='col-lg-4'>
              <div class='card card-danger'>
                <div  class='card-header'>
                  <h3 id=laporan class='card-title'>Konfigurasi Lokasi Nusa Dua </h3>
                  <div class='card-tools'>
                  
                </div>
                </div>
                <div class='card-body'>
                  <form action='../ns.php' method='post'>
                  <label>Masukkan batas kecepatan angin</label>
                  <div class="input-group ">
                   <input type="number" class="form-control" name="kec2" placeholder="Masukkan Batas Kecepatan Angin Dalam Angka" required>
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-wind"></span>
                      </div>
                      <br>
                    </div>
                   </div>
                  <div>
                   <br>
                    <button type='submit' class='btn btn-success float-right'>Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </section>
           
    
            </div>
    </div>
    </section>
        <!-- graf konten -->
  <!-- /.control-sidebar -->
  <?php include '../footer.php'?>
  <!-- Main Footer -->

</div>    
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="../plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../plugins/raphael/raphael.min.js"></script>
<!-- ChartJS -->
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>