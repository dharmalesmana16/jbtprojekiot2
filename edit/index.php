<?php 
session_start();
if (empty($_SESSION['username'])) {
    echo "<script>alert('Maaf, untuk mengakses halaman ini, anda harus login terlebih dahulu, terima kasih');document.location='../index.php'</script>";
}
if (isset($_SESSION['username'])){
$_SESSION['login_user_time']=time();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Monitoring Angin</title>
  <link rel="shortcut icon" href="Jasamarga_Bali.ico" type="image/x-icon">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../dist/css/nav.css">
  <script src="https://www.cssscript.com/demo/canvas-based-html5-gauge-library-gauge-js/gauge.min.js"></script>
</head>
<body class="hold-transition  sidebar-collapse layout-fixed layout-navbar-fixed layout-footer-fixed">
    
<div class="wrapper">
  <!-- Navbar -->
  <?php include '../header.php' ?>
  <!-- Content Wrapper. Contains page content -->

    <section class="content">
      <div class="container-fluid">
        <!-- Info laporan -->
        <div class="row">
       <div class="col-3">
       </div>

        <div class="col-6">
        <div class="card card-outline card-warning ">
    <div class="card-header text-left">
      <a class="h1"><b>Ganti Password</b></a>
    </div>
    <div class="card-body">
      <!-- <p class="text-left">Konfigurasi Password</p> -->
      <form action="../ganti_password.php" method="post">
      <input type="hidden" name="username" value="<?= $_SESSION['username'] ?>">

      <label>Masukkan Password Lama</label>  
      <div class="input-group mb-4">
      
          <input type="password" class="form-control" name="pass_lama" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <label>Masukkan Password Baru</label>
        <div class="input-group mb-4">
          <input type="password" class="form-control" name="pass_baru" placeholder="Confirm Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <label>Konfirmasi Password Baru </label>
        <div class="input-group mb-4">
          <input type="password" class="form-control" name="konfirmasi_pass" placeholder="Confirm Password" required> 
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
         <div class="col-10">

         </div>
          <div class="col-2">
            <button type="submit" class="btn btn-success btn-block float-right">Simpan</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    
      
    </div>
        <center>
        </div>
        </div>
    </section>
    
    </div>
        <!-- graf konten -->
  <!-- /.control-sidebar -->
  <?php include '../footer.php' ?>
  <!-- Main Footer -->

</div>    
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="../plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../plugins/raphael/raphael.min.js"></script>
<!-- ChartJS -->
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>